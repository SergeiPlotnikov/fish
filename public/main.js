"use strict";

window.addEventListener("DOMContentLoaded", function (event) {

    const PRICE_ONE = 200,
        PRICE_TWO = 300,
        PRICE_THREE = 400,
        NOTHING_EXTRA_PRICE = 0,
        RADIO_ONE = 10,
        RADIO_TWO = 300,
        RADIO_THREE = 3000,
        CHECKBOX = 1000;

    let price = PRICE_TWO,
        extraPrice = RADIO_ONE,
        result;

    let calculate = document.getElementById("calculate"),
        quantity = document.getElementById("quantity"),
        resultSpan = document.getElementById("result"),
        product = document.getElementById("product"),
        radiosdiv = document.getElementById("radio-div"),
        radios = document.querySelectorAll("#radio-div input[type=radio]"),
        checkdiv = document.getElementById("check-div"),
        check = document.getElementById("check");

    product.addEventListener("change", function (event) {

        let option = event.target;

        if (option.value == "1") {
            radiosdiv.classList.add("d-none");
            checkdiv.classList.add("d-none");
            extraPrice = NOTHING_EXTRA_PRICE;
            price = PRICE_ONE;
        }
        if (option.value == "2") {
            radiosdiv.classList.remove("d-none");
            checkdiv.classList.add("d-none");
            extraPrice = RADIO_ONE;
            price = PRICE_TWO;
            document.getElementById("radio1").checked = true;
        }
        if (option.value == "3") {
            checkdiv.classList.remove("d-none");
            radiosdiv.classList.add("d-none");
            extraPrice = NOTHING_EXTRA_PRICE;
            price = PRICE_THREE;
            check.checked = false;
        }
    });

    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {

            let radio = event.target;

            if (radio.value == "option1") extraPrice = RADIO_ONE;
            if (radio.value == "option2") extraPrice = RADIO_TWO;
            if (radio.value == "option3") extraPrice = RADIO_THREE;
        });
    });

    check.addEventListener("change", function (event) {

        if (check.checked) {
            extraPrice = CHECKBOX;
        }
        else {
            extraPrice = NOTHING_EXTRA_PRICE;
        }
    });

    calculate.addEventListener("change", function (event) {

        if (quantity.value < 1) quantity.value = 1;

        result = price * quantity.value + extraPrice;
        resultSpan.innerHTML = result;
    });

});

